-- SPDX-FileCopyrightText: 2021 David Hurka <doxydoxy@mailbox.org>
-- SPDX-License-Identifier: MIT OR CC0-1.0

--! @file cityimage.lua
--!
--! A command line application to generate small images with city outlines
--! from the mapdata used at https://wiki.linux-forks.de/mediawiki/Maps:Cities.
--!
--! To use this program, download the source code of the above page
--! to a local file, and run this program with the file as argument.
--! SVG images will be created for each city, in the current working directory.
--!
--! There may be more arguments. Specifically, the arguments are:
--! 1. Maps:Cities for city outlines
--! 2. Maps:Streets for streets
--! 3. Maps:BodiesOfWater for river and ocean outlines
--! 4. Maps:Regions for defining extra regions to create SVG images for
--!
--! There may be more arguments,
--! please see the main function for up-to-date informations.
--!
--! It is theoretically possible to use this code in another project.
--! All functionality is implemented inside the table @c city_image,
--! and is used by instantiating a city_image object and calling the "main" function.
--!
--! @code
--! local generator = city_image.new();
--! generator.process_file("city_data.txt");
--! @endcode

local stringy = require "stringy";

--! @class city_image
--! Program to generate city outline images from the Linux Forks wiki mapdata.
local city_image = {
    --! List of city_image.city objects.
    cities = {},
    --! List of city_image.street objects.
    streets = {},
    --! List of city_image.ocean objects.
    oceans = {},
    --! List of city_image.river objects.
    rivers = {},
    --! List of city_image.region objects.
    regions = {},
};

--! Color scheme.
--!
--! The key of each style is the @c class of a city_image.city.
--! Examples: "city", "street".
--!
--! Each style has the variants @c current, and @c other.
--! @c current is used for drawing the current city,
--! @c other is used fo other cities appearing on the same image, and streets.
--!
--! Each style variant can have these elements:
--!
--! \li @c fill, @c fill_opacity
--!     A solid fill drawn in the lowest layers.
--! \li @c stroke, @c stroke_width, @c stroke_opacity, @c stroke_dasharray
--!     A line drawn in higher layers than all fills.
--! \li @c fill_2, @c fill_opacity_2, @c stroke_2, @c stroke_width_2, @c stroke_opacity_2, @c stroke_dasharray_2
--!     A second shape drawn on top all previous.
--! \li @c waves
--!     Property table for a layer of wavy lines drawn on top of the previous shapes.
--!     Has these properties:
--!     @c color, @c opacity, @c width, @c length, @c height, @c spacing
--! \li @c fill_3, @c fill_opacity_3, @c stroke_3, @c stroke_width_3, @c stroke_opacity_3, @c stroke_dasharray_3
--!     A third shape drawn on top all others.
--!
--! Absent values for @c fill or @c stroke cause no elements to
--! be drawn on the respective layer.
--! Absent values for opacities or dasharrays cause default values to be used.
--!
--! Each style can have the attribute @c scaling.
--! If it is @c "absolute", then @c stroke_width and @c stroke_width_2 are
--! specified in map units instead of image units.
--!
--! Each style can have the attribute @c link, which is a string format which
--! will be used to hyperlink the object name.
--!
--! Each style can have the attribute @c title, which is a string format which
--! will be used to show the object name in tooltips.
--!
--! Each style can have the attribute @c image_title, which is a string format
--! which will be used to give the generated image a title.
--!
--! Each style that can create image files needs the @c image_height property,
--! which is the number of pixels the image shall have in the y-dimension.
--! For regions, the height may be specified individually in map data.
--!
--! Each style that can create image files needs the  @c file_name property,
--! which is a string format which will be used for the file name.
city_image.styles = {
    city = {
            current = {fill = "#fb8e45", stroke_3 = "#e76412", stroke_width_3 = 12},
            other = {fill = "#ffff6c", stroke_3 = "#f5f521", stroke_width_3 = 10},
            link = "https://wiki.linux-forks.de/mediawiki/index.php/%s",
            title = "%s\n(City)",
            image_title = "%s - City Outline Drawing";
            image_height = 450;
            file_name = "Outline_of_%s.svg"
           },
    town = {
            current = {fill = "#f57050", stroke_3 = "#f63b10", stroke_width_3 = 10},
            other = {fill = "#e3f772", stroke_3 = "#def70b", stroke_width_3 = 8},
            link = "https://wiki.linux-forks.de/mediawiki/index.php/%s",
            title = "%s\n(Town)",
            image_title = "%s - Town Outline Drawing";
            image_height = 450;
            file_name = "Outline_of_%s.svg"
           },
    village = {
               current = {fill = "#fa6c6c", stroke_3 = "#f23c3c", stroke_width_3 = 10},
               other = {fill = "#ddfa77", stroke_3 = "#def92b", stroke_width_3 = 8},
               link = "https://wiki.linux-forks.de/mediawiki/index.php/%s",
               title = "%s\n(Village)",
               image_title = "%s - Village Outline Drawing";
               image_height = 450;
               file_name = "Outline_of_%s.svg"
              },
    district = {
                current = {fill = "#f8a750", stroke_3 = "#fb9c14", stroke_width_3 = 7},
                other = {fill = "#ffff80", stroke_3 = "#f1f162", stroke_width_3 = 5},
                title = "%s\n(District)",
                image_title = "%s - District Outline Drawing";
                image_height = 450;
                file_name = "Outline_of_%s.svg"
               },
    highways = {
                other = {stroke_3 = "#0748ec", stroke_width_3 = 3},
                title = "%s\n(Highway)",
               },
    country_roads = {
                     other = {stroke_3 = "#0748ec", stroke_width_3 = 2},
                     title = "%s\n(Country road)",
                    },
    street = {
              other = {stroke_3 = "#2c3394", stroke_width_3 = 1.5},
              title = "%s\n(Street)",
             },
    River = {
             other = {
                      stroke = "#5d62f2", stroke_width = 25,
                      stroke_2 = "#7379ff", stroke_width_2 = 17,
                      waves = {
                               color = "#5d62f2", width = 1.5,
                               length = 18, height = 4, spacing = 10,
                              },
                     },
             scaling = "absolute",
             title = "%s\n(River)",
            },
    Brook = {
             other = {
                      stroke = "#5d62f2", stroke_width = 15,
                      stroke_2 = "#7379ff", stroke_width_2 = 9,
                      waves = {
                               color = "#5d62f2", width = 1.5,
                               length = 18, height = 4, spacing = 10,
                              },
                     },
             scaling = "absolute",
             title = "%s\n(Small river / brook)",
            },
    Passage = {
               other = {
                        stroke = "#5d62f2", stroke_width = 5,
                        stroke_2 = "#7379ff", stroke_width_2 = 2,
                       },
               scaling = "absolute",
               title = "%s\n(Waterway passage)",
              },
    Sloped = {
              other = {
                       stroke = "#8185fa", stroke_width = 12,
                       stroke_2 = "#9397ff", stroke_width_2 = 8,
                       waves = {
                                color = "#8185fa", width = 1.5,
                                length = 18, height = 6, spacing = 10,
                               },
                      },
              scaling = "absolute",
              title = "%s\n(River with significant slope)",
             },
    Anabranch = {
                 other = {
                          stroke = "#5d62f2", stroke_width = 19,
                          stroke_2 = "#7379ff", stroke_width_2 = 11,
                          waves = {
                                   color = "#5d62f2", width = 1.5,
                                   length = 18, height = 4, spacing = 10,
                                  },
                         },
                 scaling = "absolute",
                 title = "%s\n(River anabranch)",
                },
    Brook_Anabranch = {
                       other = {
                               stroke = "#5d62f2", stroke_width = 9,
                               stroke_2 = "#7379ff", stroke_width_2 = 4,
                               waves = {
                                        color = "#5d62f2", width = 1.5,
                                        length = 18, height = 4, spacing = 10,
                                       },
                              },
                      scaling = "absolute",
                      title = "%s\n(Small river’s / brook’s anabranch)",
                     },
    Sloped_Anabranch = {
                       other = {
                                stroke = "#8185fa", stroke_width = 6,
                                stroke_2 = "#9397ff", stroke_width_2 = 3,
                                waves = {
                                         color = "#8185fa", width = 1.5,
                                         length = 18, height = 4, spacing = 10,
                                        },
                               },
                       scaling = "absolute",
                       title = "%s\n(Anabranch of river with significant slope)",
                      },
    Canal = {
             other = {
                      stroke = "#8d4ceb", stroke_width = 18,
                      stroke_2 = "#9070ff", stroke_width_2 = 11,
                      waves = {
                               color = "#8d4ceb", width = 1.5,
                               length = 18, height = 4, spacing = 10,
                              },
                     },
             scaling = "absolute",
             title = "%s\n(Canal)",
            },
    Ocean = {
             other = {
                      stroke = "#5d62f2", stroke_width = 8,
                      fill_2 = "#7379ff",
                      waves = {
                               color = "#5d62f2", width = 1.5,
                               length = 18, height = 4, spacing = 10,
                              },
                     },
             scaling = "absolute",
             title = "%s\n(Ocean)",
            },
    Sea = {
           other = {
                    stroke = "#5d62f2", stroke_width = 8,
                    fill_2 = "#7379ff",
                    waves = {
                             color = "#5d62f2", width = 1.5,
                             length = 18, height = 4, spacing = 10,
                            },
                   },
           scaling = "absolute",
           title = "%s\n(Sea)",
          },
    Bay = {
           other = {
                    stroke = "#5d62f2", stroke_width = 6,
                    fill_2 = "#7379ff",
                    waves = {
                             color = "#5d62f2", width = 1.5,
                             length = 18, height = 4, spacing = 10,
                            },
                   },
           scaling = "absolute",
           title = "%s\n(Bay)",
          },
    Lagoon = {
              other = {
                       stroke = "#5d62f2", stroke_width = 6,
                       fill_2 = "#7379ff",
                       waves = {
                                color = "#5d62f2", width = 1.5,
                                length = 18, height = 4, spacing = 10,
                               },
                      },
              scaling = "absolute",
              title = "%s\n(Lagoon)",
             },
    Strait = {
              other = {
                       stroke = "#5d62f2", stroke_width = 6,
                       fill_2 = "#7379ff",
                       waves = {
                                color = "#5d62f2", width = 1.5,
                                length = 18, height = 4, spacing = 10,
                               },
                      },
              scaling = "absolute",
              title = "%s\n(Strait)",
             },
    Lake = {
             other = {
                      stroke = "#5d62f2", stroke_width = 6,
                      fill_2 = "#7379ff",
                      waves = {
                               color = "#5d62f2", width = 1.5,
                               length = 18, height = 3, spacing = 10,
                              },
                     },
             scaling = "absolute",
             title = "%s\n(Lake)",
            },
    Pond = {
             other = {
                      stroke = "#8d4ceb", stroke_width = 5,
                      fill_2 = "#9070ff",
                      waves = {
                               color = "#8d4ceb", width = 1.5,
                               length = 18, height = 2, spacing = 10,
                              },
                     },
             scaling = "absolute",
             title = "%s\n(Pond, artifical lake)",
            },
    Port = {
             other = {
                      stroke = "#97b1f5", stroke_width = 7,
                      fill_2 = "#9162ff",
                      waves = {
                               color = "#97b1f5", width = 1.5,
                               length = 18, height = 2, spacing = 10,
                              },
                     },
             scaling = "absolute",
             title = "%s\n(Port, industrial water body)",
            },
    region = {
              current = {},
              other = {},
              image_title = "%s - Region Summary Drawing";
              image_height = 600;
              file_name = "Region_%s_Summary.svg"
             },
    region_outlined = {
                       current = {
                                  stroke = "#19bf95", stroke_width = 8,
                                  stroke_dasharray = "25,15,8,15",
                                 },
                       other = {},
                       image_title = "%s - Region Summary Drawing";
                       image_height = 600;
                       file_name = "Region_%s_Summary.svg"
                      },
}


function city_image:new()
    return setmetatable({}, {__index = self});
end

--! Returns the next `<!-- comment -->` from @p comment_string,
--! starting at @p offset, or @c nil.
--!
--! Returns the whole `<!-- comment -->` substring, along with its positions.
--!
--! @returns comment_string, start_position, end_position
function city_image.next_comment(input, offset)
    local comment_start = stringy.find(input, "<!--", offset);
    if not comment_start then
        return nil;
    end

    local comment_end = stringy.find(input, "-->", comment_start);
    if not comment_end then
        return nil;
    else
        -- Length of "-->" - 1.
        comment_end = comment_end + 2;
    end

    return string.sub(input, comment_start, comment_end), comment_start, comment_end;
end

--! Returns an iterator through data tables in the string @p input.
--!
--! A data table is a usual MediaWiki table (`{| ... |}`),
--! which follows a `begin:mapdata` XML comment.
--!
--! @returns start_position, end_position
function city_image.data_sections(input)
    local offset = 1;

    local function iterator()
        local comment_text, comment_start, comment_end = city_image.next_comment(input, offset);
        if not comment_text then
            -- End of input.
            return nil;
        end

        offset = comment_end + 1;

        if not string.find(comment_text, "begin:mapdata", 1, --[[ plain ]] true) then
            -- Check next comment.
            return iterator();
        end

        local table_start = string.find(input, "{|", offset, --[[ plain ]] true);
        if not table_start then
            print("No table follows after one begin:mapdata comment.");
            return nil;
        end

        local table_end = string.find(input, "|}", table_start + 2, --[[ plain ]] true);
        if not table_end then
            print("A table following a begin:mapdata comment is not closed.");
            return nil;
        end

        offset = table_end + 2;
        return table_start + 2, table_end - 1;
    end

    return iterator;
end

--! Returns an iterator through elements of a string
--! from @p start to @p end, separated by @p separator.
--!
--! @example
--! @p separator can be @c "|-" or @c "|" to parse a MediaWiki table.
--!
--! @returns element_start_position, element_end_position
function city_image.string_elements(input, start_position, end_position, separator)
    --! The position where the next element starts in each iteration.
    local offset = start_position;

    local function iterator()
        if offset > end_position - #separator then
            -- There will not be any more element.
            return nil;
        end

        local element_start = offset;
        local separator_start = string.find(input, separator, offset,
                                            --[[ plain ]] true);
        separator_start = separator_start or end_position + 1;

        -- The end of the input string counts as separator start.
        separator_start = math.min(separator_start, end_position + 1);
        local element_end = separator_start - 1;

        offset = separator_start + #separator;

        return element_start, element_end;
    end

    return iterator;
end

--! Returns an iterator through rows of a MediaWiki table
--! from @p table_start to @p table_end (including @c {| and @c |}).
--!
--! @returns row_start_position, row_end_position
function city_image.table_rows(input, table_start, table_end)
    return city_image.string_elements(input, table_start, table_end, "|-");
end

--! Returns an iterator through cells of a MediaWiki table row
--! from @p row_start to @p row_end (excluding @c |- or @c |}).
--!
--! Tries to skip styling sections, which are usually between ordinary cells.
--! Empty cells are included, if they contain at least one space character.
--!
--! @returns cell_content (as string, whitespace stripped)
function city_image.table_cells(input, row_start, row_end)
    -- It is not expected that styling sections exist for any cells in the data table.
    -- But the check for any present styling section (by searching "=")
    -- requires a search through the whole remaining string for each cell.
    -- Therefore, the string should be short, so this iterator is working on
    -- true substrings, and therefore returns strings.
    local get_next_element = city_image.string_elements(input, row_start, row_end, "|");

    -- The row does not start until the first |, therefore discard first element.
    get_next_element();

    local function iterator()
        local cell_start, cell_end = get_next_element();

        if not cell_start then
            -- End of row.
            return nil;
        end

        if cell_end < cell_start then
            -- Empty cell (no whitespace character), try next.
            return iterator();
        end

        local cell = stringy.strip(string.sub(input, cell_start, cell_end));

        if string.find(cell, "=", 1, --[[ plain ]] true) then
            -- Probably styling field for the next cell, try next.
            return iterator();
        end

        return cell;
    end

    return iterator;
end

--! Returns the position of the last `== heading ==` ( only of this level).
--!
--! @param input The string in which to search.
--! @param start_position The position at which to begin to search.
--! @param end_position The position at which to stop to search.
--!
--! @returns heading_start_position, heading_end_position (including equal signs.)
function city_image.last_heading_before(input, start_position, end_position)
    local get_next_equals = city_image.string_elements(input, start_position, end_position, "==");

    --! Store the `==` start positions of the last verified pair.
    local last_start, last_end;

    --! Stores the `==` start position of the last verified single occurence.
    local last_single;

    -- get_next_equals returns the position after each double equal.
    -- It also returns the beginning as start of the first element. Skip that.
    get_next_equals();
    local s = get_next_equals();

    while s and s < end_position - 1 do
        if (string.sub(input, s - 3, s - 3) ~= "="
            and string.sub(input, s, s) ~= "=") then
            -- This is a double (not triple) equal sign.

            if last_single then
                -- Found a pair.
                last_start, last_end = last_single, s;
            end

            last_single = s;
        else
            -- This is at least a triple equal sign, this heading does not count.
            last_single = nil;
        end

        s = get_next_equals();
    end

    -- Reached end_position.
    if last_start then
        -- Include the equal signs.
        return last_start - 2, last_end - 1;
    else
        return nil;
    end
end

--! Parses one table row of mapdata,
--! and appends a city object to #cities at success.
function city_image:parse_city_row(input, row_start, row_end)
    local strings = {};
    for s in city_image.table_cells(input, row_start, row_end) do
        table.insert(strings, s);
    end

    if #strings ~= 3 then
        local skipped_string = stringy.strip(string.sub(input, row_start, row_end));
        local silent = (#strings == 0
                        or stringy.startswith(skipped_string, "class=\"")
                        or string.find(strings[1], "!!", 1, --[[ plain ]] true));
        if not silent then
            print("Skipping table row \"" .. string.sub(input, row_start, row_end) .. "\".");
        end

        return;
    end

    local name, class, coordinates = strings[1], strings[2], strings[3];

    local multi_shape = city_image.multi_polygon:new();
    self.parse_coordinates(coordinates, multi_shape, city_image.polygon);

    if multi_shape:area() == 0 then
        print("City " .. name .. " has zero area.");
        return;
    end

    local city = city_image.city:new(name, class, multi_shape);
    table.insert(self.cities, city);
end

--! Parses one table row of mapdata,
--! and appends a street object of class @p class to #streets at success.
function city_image:parse_street_row(input, row_start, row_end, class)
    local strings = {};
    for s in city_image.table_cells(input, row_start, row_end) do
        table.insert(strings, s);
    end

    if #strings ~= 2 then
        local skipped_string = stringy.strip(string.sub(input, row_start, row_end));
        local silent = (#strings == 0
                        or stringy.startswith(skipped_string, "class=\"")
                        or string.find(strings[1], "!!", 1, --[[ plain ]] true));
        if not silent then
            print("Skipping table row \"" .. string.sub(input, row_start, row_end) .. "\".");
        end

        return;
    end

    local name, coordinates = strings[1], strings[2];

    local multi_shape = city_image.multi_polyline:new();
    self.parse_coordinates(coordinates, multi_shape, city_image.polyline);

    if multi_shape:point_count() == 0 then
        print("Street " .. name .. " has zero length.");
        return;
    end

    local street = city_image.street:new(name, class, multi_shape);
    table.insert(self.streets, street);
end

--! Parses one table row of mapdata,
--! and appends a water body object to #rivers or #oceans at success.
--! @p shape_type is "Oceans, Seas, and Lakes", or "Rivers".
function city_image:parse_water_row(input, row_start, row_end, shape_type)
    local shape_prototype, multishape_prototype, shape_list;
    if shape_type == "Oceans, Seas, and Lakes" then
        shape_prototype = city_image.polygon;
        multishape_prototype = city_image.multi_polygon;
        water_prototype = city_image.ocean;
        shape_list = city_image.oceans;
    elseif shape_type == "Rivers" then
        shape_prototype = city_image.polyline;
        multishape_prototype = city_image.multi_polyline;
        water_prototype = city_image.river;
        shape_list = city_image.rivers;
    else
        error("Invalid water body type:" .. shape_type);
    end

    local strings = {};
    for s in city_image.table_cells(input, row_start, row_end) do
        table.insert(strings, s);
    end

    if #strings ~= 3 then
        local skipped_string = stringy.strip(string.sub(input, row_start, row_end));
        local silent = (#strings == 0
                        or stringy.startswith(skipped_string, "class=\"")
                        or string.find(strings[1], "!!", 1, --[[ plain ]] true));
        if not silent then
            print("Skipping table row \"" .. string.sub(input, row_start, row_end) .. "\".");
        end

        return;
    end

    local name, class, coordinates = strings[1], strings[2], strings[3];

    local multi_shape = multishape_prototype:new();
    self.parse_coordinates(coordinates, multi_shape, shape_prototype);

    if multi_shape:point_count() == 0 then
        print("Water body " .. name .. " has zero length.");
        return;
    end

    local water_body = water_prototype:new(name, class, multi_shape);

    -- River objects need an additional outline object for determining
    -- the area where to paint waves.
    if water_prototype == city_image.river then
        water_body:add_stroke_outline(self.styles.River.other.stroke_width_2);
    end

    table.insert(shape_list, water_body);
end

--! Parses one table row of mapdata,
--! and appends a region object to #regions at success.
function city_image:parse_region_row(input, row_start, row_end)
    local strings = {};
    for s in city_image.table_cells(input, row_start, row_end) do
        table.insert(strings, s);
    end

    if #strings ~= 7 then
        local skipped_string = stringy.strip(string.sub(input, row_start, row_end));
        local silent = (#strings == 0
                        or stringy.startswith(skipped_string, "class=\"")
                        or string.find(strings[1], "!!", 1, --[[ plain ]] true));
        if not silent then
            print("Skipping table row \"" .. string.sub(input, row_start, row_end) .. "\".");
        end

        return;
    end

    local name, class, image_height, stroke_scale, param1, param2, coordinates = strings[1], strings[2], strings[3], strings[4], strings[5], strings[6], strings[7];

    local multi_shape = city_image.multi_polygon:new();
    self.parse_coordinates(coordinates, multi_shape, city_image.polygon);

    -- Zero area regions are allowed and sensible.
    --[[
    if multi_shape:area() == 0 then
        print("Region " .. name .. " has zero area.");
        return;
    end
    ]]

    local region = city_image.region:new(name, class, image_height, stroke_scale, param1, param2, multi_shape);
    table.insert(self.regions, region);
end

--! Parses one row of coordinates (in brackets) from @p input
--! and appends shapes to @p multi_shape (e. g. a multi_polyline).
--!
--! @p shape_prototype is the prototype used to create shapes.
--!
--! Invalid coordinates are discarded.
function city_image.parse_coordinates(input, multi_shape, shape_prototype)
    -- Make coordinate string iterator
    -- and discard everything up to the first opening bracket:
    local get_next_coordinate = city_image.string_elements(input, 1, #input, "[");
    get_next_coordinate();

    local current_shape = shape_prototype:new();

    for s, e in get_next_coordinate do
        local coordinate_string = string.sub(input, s - 1, e);

        local success, coordinate, end_of_subshape = pcall(city_image.parse_coordinate, coordinate_string);
        if not success then
            local error_message = coordinate;
            print("Can not use coordinate \"" .. coordinate_string .. "\": " .. error_message);
        elseif coordinate then
            local success, error_message = pcall(current_shape.append, current_shape, coordinate);
            if not success then
                print("Can not use coordinate \"" .. coordinate_string .. "\": " .. error_message);
            end

            if end_of_subshape then
                multi_shape:add_shape(current_shape);
                current_shape = shape_prototype:new();
            end
        end
    end

    -- Coordinate lists are not expected to end with ]].
    -- The last shape usually needs to be added to the shape list here:
    if #current_shape.data > 0 then
        multi_shape:add_shape(current_shape);
    end
end

--! Parses one atom of mapdata, which is one coordinate in brackets.
--!
--! If the coordinate contains a second closing bracket,
--! the end of the current subshape is reported.
--!
--! If the input is just an opening bracket [,
--! it shall be skipped and this function returns nothing.
--!
--! @exception _ Invalid coordinate string.
--!
--! @returns Table with fields @p x and @p y, end_of_subshape; or nothing.
function city_image.parse_coordinate(input)
    local left_bracket = string.find(input, "[", 1, --[[ plain ]] true);
    if not left_bracket then
        error("Can not parse coordinate \"" .. input .. "\": opening bracket missing.");
    end

    local first_comma = string.find(input, ",", left_bracket + 1, --[[ plain ]] true);
    if not first_comma then
        if left_bracket == #input then
            -- This is the beginning of a new subshape,
            -- and the coordinate iterator found the first [ in a ]],[[ separator.
            return
        else
            error("Can not parse coordinate \"" .. input .. "\": comma missing.");
        end
    end

    -- Search from first bracket to catch coordinates that are closed too early.
    local right_bracket = string.find(input, "]", left_bracket + 1, --[[ plain ]] true);
    if not right_bracket then
        error("Can not parse coordinate \"" .. input .. "\": bracket missing.");
    elseif right_bracket < first_comma then
        error("Can not parse coordinate \"" .. input .. "\": comma missing.");
    end

    -- Coordinates are allowed to have a height number in the middle.
    -- But the second comma may also be the separator to the next coordinate.
    local second_comma = string.find(input, ",", first_comma + 1, --[[ plain ]] true);
    if not second_comma or (second_comma > right_bracket) then
        second_comma = first_comma;
    end

    -- Assemble coordinate result.
    local x = string.sub(input, left_bracket + 1, first_comma - 1);
    local y = string.sub(input, second_comma + 1, right_bracket - 1);
    local coordinate = {x = x, y = y};

    -- Check whether this is the end of a subshape.
    -- Subshapes end with ]],[[, so we need to check if there is a second ].
    local second_right_bracket = string.find(input, "]", right_bracket + 1, --[[ plain ]] true);
    local end_of_subshape = second_right_bracket;

    return coordinate, end_of_subshape;
end

--! Parses the whole input string as MediaWiki markup.
function city_image:parse_city_input(input)
    for s, e in self.data_sections(input) do
        for s, e in self.table_rows(input, s, e) do
            self:parse_city_row(input, s, e);
        end
    end
end

--! Parses the whole input string as MediaWiki markup.
function city_image:parse_street_input(input)
    -- Street classes are defined by level 2 headings.
    -- Keep track of these headings using last_heading_before().
    local last_data_section_end = 1;
    local class = "street";

    for s, e in self.data_sections(input) do
        do
            -- Find the last heading before this data section (MediaWiki table).
            local s, e = self.last_heading_before(input, last_data_section_end, s);
            if s then
                class = stringy.strip(string.lower(string.sub(input, s + 2, e - 2)));
                if class == "map data" then
                    -- Default class.
                    class = "street"
                else
                    class = string.gsub(class, " ", "_");
                end
            end
        end

        for s, e in self.table_rows(input, s, e) do
            self:parse_street_row(input, s, e, class);
        end

        last_data_section_end = e;
    end
end


--! Parses the whole input string as MediaWiki markup.
function city_image:parse_water_input(input)
    -- Water body shape types are defined by level 2 headings.
    -- Keep track of these headings using last_heading_before().
    local last_data_section_end = 1;
    local shape_type;

    for s, e in self.data_sections(input) do
        do
            -- Find the last heading before this data section (MediaWiki table).
            local s, e = self.last_heading_before(input, last_data_section_end, s);
            if s then
                shape_type = stringy.strip(string.sub(input, s + 2, e - 2));
            end
        end

        for s, e in self.table_rows(input, s, e) do
            self:parse_water_row(input, s, e, shape_type);
        end

        last_data_section_end = e;
    end
end

--! Parses the whole input string as MediaWiki markup.
function city_image:parse_regions_input(input)
    for s, e in self.data_sections(input) do
        for s, e in self.table_rows(input, s, e) do
            self:parse_region_row(input, s, e);
        end
    end
end

--! Calculates an almost square bounding rectangle with some padding for @p object.
--!
--! @returns {xmin, xmax, ymin, ymax} as table.
function city_image.calculate_bounds_for_object(object)
    local bounds = object.outline:bounds();

    local size = {x = bounds.xmax - bounds.xmin, y = bounds.ymax - bounds.ymin};

    -- Grow bounds to get some padding.
    local padding = math.max(size.x, size.y) * 0.15
    bounds.xmin = bounds.xmin - padding;
    bounds.xmax = bounds.xmax + padding;
    bounds.ymin = bounds.ymin - padding;
    bounds.ymax = bounds.ymax + padding;
    size = {x = bounds.xmax - bounds.xmin, y = bounds.ymax - bounds.ymin};

    -- Grow bounds to get almost square shape.
    local ratio = (size.x / size.y) or math.huge;
    if ratio < 0.85 then
        local width_needed = size.y * 0.85;
        local padding_x = (width_needed - size.x) / 2;
        bounds.xmin = bounds.xmin - padding_x;
        bounds.xmax = bounds.xmax + padding_x;
    elseif ratio > 1.4 then
        local height_needed = size.x / 1.4;
        local padding_y = (height_needed - size.y) / 2;
        bounds.ymin = bounds.ymin - padding_y;
        bounds.ymax = bounds.ymax + padding_y;
    end

    return bounds;
end

--! Calculates transformation parameters for one component
--! that maps linearly from @p a to @p a_ and from @p b to @p b_.
--!
--! @exception _ If no transformation is possible.
--!
--! @returns r, s as list, so that `a' = a * r + s`.
function city_image.calculate_component_transform(a, b, a_, b_)
    --! Inverse of singular transformation requested?
    if a == b and a_ ~= b_ then
        error("Linear transformation from (" .. a .. ", " .. b .. ") to (" .. a_ .. ", " .. b_ .. ") not possible.");
    end

    if a == b then
        print("Linear transformation from (" .. a .. ", " .. b .. ") to (" .. a_ .. ", " .. b_ .. ") is ambiguous, using constant offset.");
        return 1, a_ - a;
    end

    local scale = (a_ - b_) / (a - b);
    return scale, a_ - a * scale;
end

--! Calculates a transformation matrix that maps @p rect_in to @p rect_out.
--!
--! @p rect_in and @p rect_out must have fields @p xmin, ..., @p ymax.
--!
--! @exception _ If @c rect_in has zero width or height.
--!
--! @returns {m11, ... m33} as table (@c m<row><column>),
--!          so that `(x' * k, y' * k, k) = (x, y, 1) * M`.
function city_image.calculate_transform(rect_in, rect_out)
    -- Identity matrix.
    local transform = {m11 = 1, m12 = 0, m13 = 0,
                       m21 = 0, m22 = 1, m23 = 0,
                       m31 = 0, m32 = 0, m33 = 1};

    -- Adapt the matrix with two component-wise transformations.
    transform.m11, transform.m31 = city_image.calculate_component_transform(
        rect_in.xmin, rect_in.xmax, rect_out.xmin, rect_out.xmax);
    transform.m22, transform.m32 = city_image.calculate_component_transform(
        rect_in.ymin, rect_in.ymax, rect_out.ymin, rect_out.ymax);

    return transform;
end

--! Returns whether rectangles @p a and @p b intersect.
--! @p a and @p b must have fieds @p xmin, ..., @p ymax.
function city_image.rectangles_intersect(a, b)
    return a.xmin <= b.xmax and a.xmax >= b.xmin
            and a.ymin <= b.ymax and a.ymax >= b.ymin;
end

--! Returns @p point transformed by @p transform.
function city_image.transform_point(point, transform)
    local p, m = point, transform;

    local projection = p.x * m.m13 + p.y * m.m23 + m.m33;
    local x_ = (p.x * m.m11 + p.y * m.m21 + m.m31) / projection;
    local y_ = (p.x * m.m12 + p.y * m.m22 + m.m32) / projection;

    return {x = x_, y = y_};
end

--! Returns @p rect transformed by @p transform, and normalized (unflipped).
function city_image.transform_rect(rect, transform)
    local p1 = {x = rect.xmin, y = rect.ymin};
    local p2 = {x = rect.xmax, y = rect.ymax};

    p1 = city_image.transform_point(p1, transform);
    p2 = city_image.transform_point(p2, transform);

    return {xmin = math.min(p1.x, p2.x), xmax = math.max(p1.x, p2.x), ymin = math.min(p1.y, p2.y), ymax = math.max(p1.y, p2.y)};
end

--! Converts the coordinate system from Minetest coordinates to SVG coordinates,
--! i. e. flips the y coordinate.
function city_image:convert_coordinate_system()
    local matrix = {m11 = 1, m12 = 0, m13 = 0,
                    m21 = 0, m22 = -1, m23 = 0,
                    m31 = 0, m32 = 0, m33 = 1};

    for _, object_list in ipairs({self.cities, self.streets, self.rivers, self.oceans, self.regions}) do
        for _, object in ipairs(object_list) do
            object:transform(matrix);
        end
    end
end

--! Creates one fill and one outline element as SVG element string.
--!
--! @param object The object to paint. Needs attributes @c outline and @c class.
--! @param state "current" or "other".
--! @param absolute_scale The scaling factor for the current image.
--! @param relative_scale
--!     The stroke scaling factor for the current primary object, if present.
--! @param options Table with additional options, like @c no_waves.
--!
--! @exception _ Requested class and state not defined.
--!
--! @returns A table of 8 values, where the last is the top-most layer.
function city_image:create_image_element_stack(object, state, absolute_scale, relative_scale, options)
    local outline = object.outline;
    local class = object.class;

    local style = self.styles[class];
    if not style then
        error("No style known for class " .. class);
    end

    local sc = 1;
    if style.scaling == "absolute" then
        sc = absolute_scale;
    elseif relative_scale then
        sc = relative_scale;
    end

    local link = style.link;
    if link then
        link = self:make_link(link, object.name);
    end

    local title = style.title;
    if title then
        title = self:make_title(title, object.name);
    end

    style = style[state];
    if not style then
        error("No style known for class " .. class .. " in state " .. state);
    end

    -- Layers.
    local a, b, c, d, e;
    if style.fill then
        a = self:create_polygon_element(
            outline, title, link,
            style.fill, style.fill_opacity);
    end
    if style.stroke then
        b = self:create_polygon_element(
            outline, title, link,
            "none", nil,
            style.stroke, (style.stroke_width or 0) * sc,
            style.stroke_opacity, style.stroke_dasharray);
    end
    if style.fill_2 or style.stroke_2 then
        c = self:create_polygon_element(
            outline, title, link,
            (style.fill_2 or "none"), style.fill_opacity_2,
            style.stroke_2, (style.stroke_width_2 or 0) * sc,
            style.stroke_opacity_2, style.stroke_dasharray_2);
    end
    if style.waves and not options.no_waves then
        local outline = object.stroke_outline and object:stroke_outline() or outline;
        d = self:create_waves_element(
            outline, title, link,
            style.waves.color, style.waves.width, style.waves.opacity,
            style.waves.length, style.waves.height, style.waves.spacing);
    end
    if style.fill_3 or style.stroke_3 then
        e = self:create_polygon_element(
            outline, title, link,
            (style.fill_3 or "none"), style.fill_opacity_3,
            style.stroke_3, (style.stroke_width_3 or 0) * sc,
            style.stroke_opacity_3, style.stroke_dasharray_3);
    end

    if state == "current" then
        return {nil, a, nil, nil, nil, nil, b, c, d, e};
    else
        return {a, nil, b, c, d, e, nil, nil, nil, nil};
    end
end

--! Creates a MediaWiki link from @p pattern and @p page_name.
function city_image:make_link(pattern, page_name)
    local replacement_table = {
        [" "] = "_";
    };
    local escaped = string.gsub(page_name, "( )", replacement_table)
    return string.format(pattern, escaped);
end

--! Creates a tooltip text from @p pattern and @p object_name.
--! This function exists for the case that more sophisticated titles are
--! requested in the future.
function city_image:make_title(pattern, object_name)
    return string.format(pattern, object_name)
end

--! Creates an escaped version of @p unescaped with entities for @c < and @c &.
function city_image:xml_escaped(unescaped)
    local replacement_table = {
        ["<"] = "&lt;";
        ["&"] = "&amp;";
    };
    local escaped = string.gsub(unescaped, "([<&])", replacement_table)
    return escaped;
end

--! Creates an SVG path element in the shape of @p polygon.
--!
--! If @p polygon is empty, returns an empty string.
--!
--! @param polygon The shape to use.
--! @param title A tooltip text to be added to the polygon (optional).
--! @param link An URL to which to link the polygon (optional).
--! @param fill_color The fill color to use, as string.
--! @param fill_opacity The fill opacity to use, as number between 0 and 1. Default: 1.
--! @param stroke_color The outline color to use, as string. Default: none.
--! @param stroke_width The outline width to use. Default: 1.
--! @param stroke_opacity The outline opacity to use, as number between 0 and 1. Default: 1.
--! @param stroke_dasharray The dasharray to use. Default: none.
--!
--! @returns path element as string, indented and with line break.
function city_image:create_polygon_element(polygon, title, link, fill_color, fill_opacity, stroke_color, stroke_width, stroke_opacity, stroke_dasharray)
    local fc = fill_color;
    local fo = tonumber(fill_opacity) or 1;
    local sc = stroke_color or "none";
    local sw = tonumber(stroke_width) or 1;
    local sd = stroke_dasharray;
    local so = tonumber(stroke_opacity) or 1;

    local path_data = polygon:to_pathdata_string();
    if (#path_data < 1) then
        return "";
    end

    local attributes = {};

    if fo ~= 0 then
        table.insert(attributes, "fill=\"" .. fc .. "\"");
    end

    if fo ~= 1 then
        table.insert(attributes, string.format("fill-opacity=\"%.2f\"", fo));
    end

    if sc ~= "none" and sw ~= 0 and so ~= 0 then
        table.insert(attributes, "stroke=\"" .. sc .. "\"");
    end

    if sc ~= "none" and sw ~= 1 and so ~= 0 then
        table.insert(attributes, string.format("stroke-width=\"%.6f\"", sw));
    end

    if sc ~= "none" and sw ~= 0 and so ~= 1 then
        table.insert(attributes, string.format("stroke-opacity=\"%.2f\"", so));
    end

    if sc ~= "none" and sw ~= 0 and so ~= 0 and sd then
        table.insert(attributes, "stroke-dasharray=\"" .. sd .. "\"");
    end

    local svg_data = [[
    <path %s d="%s"/>
]];

    if link and title then
        local a_element = [[
    <a xlink:href="%s">
        <title role="tooltip">%s</title>
    %s    </a>
]];
        svg_data = string.format(a_element, self:xml_escaped(link), self:xml_escaped(title), svg_data);
    elseif link then
        local a_element = [[
    <a xlink:href="%s">
    %s    </a>
]];
        svg_data = string.format(a_element, self:xml_escaped(link), svg_data);
    elseif title then
        local g_element = [[
    <g>
        <title role="tooltip">%s</title>
    %s    </g>
]];
        svg_data = string.format(g_element, self:xml_escaped(title), svg_data);
    end

    svg_data = string.format(svg_data, table.concat(attributes, " "), path_data);

    return svg_data;
end

--! Creates an SVG group element containing some path elements which
--! form waves which fill @p polygon.
--!
--! If @p polygon is empty, returns an empty string.
--!
--! @param polygon The shape to use.
--! @param title A tooltip text to be added to the wave element (optional).
--! @param link An URL to which to link the wave element (optional).
--! @param stroke_color The outline color to use, as string.
--! @param stroke_width The outline width to use.
--! @param stroke_opacity The outline opacity to use, as number between 0 and 1. Default: 1.
--! @param wave_length The length of a single wave.
--! @param wave_height The height of a single wave, excluding @p stroke_width.
--! @param wave_spacing The vertical interval between waves.
--!
--! @returns path element as string, indented and with line break.
function city_image:create_waves_element(polygon, title, link, stroke_color, stroke_width, stroke_opacity, wave_length, wave_height, wave_spacing)
    local attributes = [[fill="none" stroke="%s" stroke-width="%.6f" stroke-opacity="%.2f"]];
    attributes = string.format(attributes, stroke_color, stroke_width, stroke_opacity or 1);

    local group_element;

    if link and title then
        local a_element = [[
    <a xlink:href="%s" %s>
        <title role="tooltip">%s</title>
%%s    </a>
]];
        group_element = string.format(a_element, self:xml_escaped(link), attributes, self:xml_escaped(title));
    elseif link then
        local a_element = [[
    <a xlink:href="%s" %s>
%%s    </a>
]];
        group_element = string.format(a_element, self:xml_escaped(link), attributes);
    elseif title then
        local g_element = [[
    <g %s>
        <title role="tooltip">%s</title>
%%s    </g>
]];
        group_element = string.format(g_element, attributes, self:xml_escaped(title));
    else
        local g_element =  [[
    <g %s>
%%s    </g>
]];
        group_element = string.format(g_element, attributes);
    end

    local bounds = polygon:bounds();

    local first_line = math.ceil(bounds.ymin / wave_spacing);
    local last_line = math.floor(bounds.ymax / wave_spacing);

    local line_elements = {};

    for line = first_line, last_line do
        local line_position = line * wave_spacing;
        local line_stops = polygon:intersections_with_infinite_line({x = 0, y = line_position}, {x = 1, y = line_position});
        assert(#line_stops % 2 == 0);

        for line_part = 1, #line_stops, 2 do
            local wave_start = {x = line_stops[line_part], y = line_position};
            local wave_end = {x = line_stops[line_part + 1], y = line_position};
            local pathdata = self:create_waves_pathdata(wave_start, wave_end, wave_length, wave_height, {x = 0, y = 0}, {x = 0, y = 1})

            if pathdata then
                local element = [[
        <path d="%s"/>
]];
                element = string.format(element, pathdata);
                table.insert(line_elements, element);
            end
        end
    end

    if not next(line_elements) then
        return "";
    end

    group_element = string.format(group_element, table.concat(line_elements));

    return group_element;
end

--! Creates an SVG pathdata string for a wavy, sine like line
--! along the line segment A (a1 -> a2).
--!
--! A line O (o1 -> o2) sets the sine wave origin where it intersects A.
--!
--! The generated path may be a bit shorter than the segment A,
--! because only whole quadrants are generated.
--!
--! Returns @c nil if no waves could fit on the segment A.
function city_image:create_waves_pathdata(a1, a2, wave_length, wave_height, o1, o2)
    -- If wave segment is too short, math gets weird (division by zero etc.).
    local v_A = self:vector(a1, a2);
    local A_squared = self:dot(v_A, v_A);
    if A_squared < (wave_length * wave_length / 16) then
        return nil;
    end

    local v_A = self:unit_vector(v_A);

    -- Calculate wave origin via t_A = det(O, a1_o1) / det(O, A):
    local wave_origin;
    do
        local v_O = self:vector(o1, o2);
        local v_a1_o1 = self:vector(a1, o1);
        local det_O_a1_o1 = self:det(v_O, v_a1_o1);
        local det_O_A = self:det(v_O, v_A);
        local t_A = det_O_a1_o1 / det_O_A;
        wave_origin = {x = a1.x + t_A * v_A.x, y = a1.y + t_A * v_A.y};
    end

    -- Calculate a line B as normalized and aliged version of A:
    local b1 = wave_origin;
    local b2 = {x = b1.x + v_A.x, y = b1.y + v_A.y};

    -- Calculate wave start and end in terms of position along line B:
    local wave_start = self:position_along_line(b1, b2, a1);
    local wave_end = self:position_along_line(b1, b2, a2);

    -- Calculate wave quadrant numbers:
    local first_quadrant = math.ceil(wave_start * 4 / wave_length);
    local last_quadrant = math.floor(wave_end * 4 / wave_length) - 1;

    if last_quadrant < first_quadrant then
        return nil;
    end

    -- Calculate wave base vectors.
    -- A wave length is divided in 12 nodes/handles,
    -- the amplitude is divided in 4 nodes/handles.
    local wave_length_12 = {x = v_A.x * wave_length / 12, y = v_A.y * wave_length / 12};
    local wave_height_4 = {x = -v_A.y * wave_height / 4, y = v_A.x * wave_height / 4};

    -- Returns a list of 3 points, which make up a cubic to movement,
    -- in order along the path.
    -- quadrant zero starts at the origin.
    local function make_quadrant(quadrant)
        local l, h = wave_length_12, wave_height_4;

        -- Draft a sine wave quadrant in a 1 unit x 1 unit grid:
        local points = {};
        points[1] = {x = 3 * quadrant + 1};
        points[2] = {x = 3 * quadrant + 2};
        points[3] = {x = 3 * quadrant + 3};
        if quadrant % 4 == 1 then
            -- First sine quadrant.
            points[1].y = -1;
            points[2].y = -2;
            points[3].y = -2;
        elseif quadrant % 4 == 2 then
            -- Second sine quadrant.
            points[1].y = -2;
            points[2].y = -1;
            points[3].y = 0;
        elseif quadrant % 4 == 3 then
            -- Third sine quadrant.
            points[1].y = 1;
            points[2].y = 2;
            points[3].y = 2;
        else
            -- Fourth sine quadrant.
            points[1].y = 2;
            points[2].y = 1
            points[3].y = 0;
        end

        -- Convert draft to actual values:
        for _, point in ipairs(points) do
            local x = l.x * point.x + h.x * point.x + wave_origin.x;
            local y = l.y * point.y + h.y * point.y + wave_origin.y;
            point.x = x;
            point.y = y;
        end

        return points;
    end

    local generated_segments = {};

    -- Generate the moveto (absolute) command at the path start:
    local first_point = make_quadrant(first_quadrant - 1)[3];
    local path_start = string.format("M %.2f %.2f", first_point.x, first_point.y);
    table.insert(generated_segments, path_start);

    -- Generate cubicto (absolute) commands for quadrants:
    for quadrant = first_quadrant, last_quadrant do
        local points = make_quadrant(quadrant);
        local segment = "C %.2f %.2f %.2f %.2f %.2f %.2f";
        segment = string.format(segment, points[1].x, points[1].y, points[2].x, points[2].y, points[3].x, points[3].y);
        table.insert(generated_segments, segment);
    end

    return table.concat(generated_segments, " ");
end

--! Creates an SVG file @c Outline_of_<name>.svg.
--!
--! Coordinates of @p city, @p other_cities, and @p bounds
--! are considered SVG element's coordinates.
--!
--! @param primary_object Determines file name etc. but is not painted.
--! @param active_objects Are painted in the active state.
--! @param other_objects Are painted in the other state.
--! @param bounds For the @c viewBox attributes.
--! @param scale The scaling factor for the current image.
--!
--! @exception _ Error at file access.
function city_image:create_image(primary_object, active_objects, other_objects, bounds, scale)
    local name = primary_object.name;
    local name_escaped = string.gsub(name, " ", "_");
    local class = primary_object.class;
    local file_name = string.format(self.styles[class].file_name, name_escaped);
    local title = string.format(self.styles[class].image_title, name);
    local stroke_scale = primary_object.stroke_scale;
    local options = primary_object.options or {};

    local viewBox_value = string.format("%i %i %i %i", bounds.xmin, bounds.ymin, bounds.xmax - bounds.xmin, bounds.ymax - bounds.ymin);

    -- List of tables: {shape: {layer: ..., layer: ..., ...}, ...}
    local shapes = {};

    for _, what in ipairs({"cities", "streets", "oceans", "rivers", "regions"}) do
        for _, o in ipairs(other_objects[what]) do
            table.insert(shapes, self:create_image_element_stack(o, "other", scale, stroke_scale, options));
        end
    end

    for _, what in ipairs({"cities", "streets", "oceans", "rivers", "regions"}) do
        for _, o in ipairs(active_objects[what]) do
            table.insert(shapes, self:create_image_element_stack(o, "current", scale, stroke_scale, options));
        end
    end

    -- Unwind element stacks:
    local elements = {};
    for layer = 1, 10 do
        for shape = 1, #shapes do
            if shapes[shape][layer] then
                table.insert(elements, shapes[shape][layer]);
            end
        end
    end

    local svg_data = [[
<!DOCTYPE svg>
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" viewBox="%s" version="1.1" fill-rule="evenodd" stroke-linejoin="round" stroke-linecap="round">
    <title>%s</title>
%s</svg>
]];

    svg_data = string.format(svg_data, viewBox_value, self:xml_escaped(title), table.concat(elements));

    local svg_file, error_message = io.open(file_name, "w");
    if not svg_file then
        error("Can not open " .. file_name .. ": " .. error_message);
    end

    svg_file:write(svg_data);
    svg_file:close();
end

--! Returns @p rect formatted with interval notation for both dimensions.
function city_image.rect_to_string(rect)
    return string.format("([% 5.1f, % 5.1f], [% 5.1f, % 5.1f])", rect.xmin, rect.xmax, rect.ymin, rect.ymax);
end

--! Creates an SVG file depicting the city at index @p index.
--!
--! Automatically calculates clip region and such stuff.
--!
--! @exception _ Error at file access.
function city_image:process_city(index)
    local primary_object = self.cities[index];
    local active_objects = {cities = {primary_object}};

    local other_objects = {
        cities = {};
        streets = self.streets;
        oceans = self.oceans;
        rivers = self.rivers;
        regions = self.regions;
    }

    for i = 1, #self.cities do
        if i ~= index then
            table.insert(other_objects.cities, self.cities[i]);
        end
    end

    self:process_object(primary_object, active_objects, other_objects);
end

--! Creates an SVG file depicting the region at index @p index.
--!
--! Automatically calculates clip region, highlighted cities, and such stuff.
--!
--! @exception _ Error at file access.
function city_image:process_region(index)
    local primary_object = self.regions[index];

    local active_objects = {
        cities = {};
        streets = {};
        oceans = {};
        rivers = {};
        regions = {};
    };
    local other_objects = {
        cities = {};
        streets = {};
        oceans = {};
        rivers = {};
        regions = {};
    };

    for _, what in ipairs({"cities", "streets", "oceans", "rivers", "regions"}) do
        for _, object in ipairs(self[what]) do
            if object == primary_object or primary_object.highlighted_objects[object.name] then
                table.insert(active_objects[what], object);
            else
                table.insert(other_objects[what], object);
            end
        end
    end

    self:process_object(primary_object, active_objects, other_objects);
end

--! Function for common functionality of process_city and process_region.
--!
--! process_city and process_region determine which objects to include.
--! This function calculates an image bounding box and coordinate transform,
--! and draws the objects in an SVG file.
--!
--! @param primary_object Determines file name etc. but is not painted.
--! @param active_objects Are painted in the active state.
--! @param other_objects Are painted in the other state.
--!
--! @p active_objects and @p other_objects are tables containing
--! these elements: @c cities, @c streets, @c oceans, @c rivers, @c regions;
--! which are lists of the respective objects.
--!
--! @exception _ Error at file access.
function city_image:process_object(primary_object, active_objects, other_objects)
    -- Calculate image bounding box and transformation:
    local map_rect = self.calculate_bounds_for_object(primary_object);
    local ratio = (map_rect.xmax - map_rect.xmin) / (map_rect.ymax - map_rect.ymin);
    local image_height = primary_object.image_height or self.styles[primary_object.class].image_height;
    local image_rect = {xmin = 0, xmax = image_height * ratio, ymin = 0, ymax = image_height};
    local transform = self.calculate_transform(map_rect, image_rect);
    local inverse_transform = self.calculate_transform(image_rect, map_rect);
    local scale = transform.m11;

    -- Round image_rect up, so the image aligns to pixels also at the right.
    image_rect.xmax = math.ceil(image_rect.xmax);

    -- Clipping is a bit bigger than the image, so borders are not cut off,
    -- and do not appear squeezed in the edges.
    local image_clip = {xmin = image_rect.xmin - 30, xmax = image_rect.xmax + 30,
                        ymin = image_rect.ymin - 30, ymax = image_rect.ymax + 30};
    local map_clip = self.transform_rect(image_clip, inverse_transform);

    -- Get transformed copies of objects to be painted.
    local active_objects_transformed = {
        cities = {};
        streets = {};
        oceans = {};
        rivers = {};
        regions = {};
    };
    local other_objects_transformed = {
        cities = {};
        streets = {};
        oceans = {};
        rivers = {};
        regions = {};
    };

    local function copy_transformed_object(object, table_to_insert_to)
        if self.rectangles_intersect(object:bounds_cached(), map_clip) then
            local transformed_object = object:clipped(map_clip):transformed(transform);
            table.insert(table_to_insert_to, transformed_object);
        end
    end

    for _, what in ipairs({"cities", "streets", "oceans", "rivers", "regions"}) do
        if active_objects[what] then
            for _, object in ipairs(active_objects[what]) do
                copy_transformed_object(object, active_objects_transformed[what]);
            end
        end

        if other_objects[what] then
            for _, object in ipairs(other_objects[what]) do
                copy_transformed_object(object, other_objects_transformed[what]);
            end
        end
    end

    -- Paint all that stuff.
    self:create_image(primary_object, active_objects_transformed, other_objects_transformed, image_rect, scale);
end

--! Calculates the determinant of vectors @p a and @p b.
--! Same as the z component of the vector product <tt>a x b</tt>.
--!
--! @p options is optional.
--!
--! If @c options.round is true, a relative error test is applied
--! to round the determinand towards zero.
--! This is useful when calculating angles between the vectors @p a and @p b.
function city_image:det(a, b, options)
    local determinant = a.x * b.y - a.y * b.x;

    if options and options.round then
        local length_A = self:dot(a, a);
        local length_B = self:dot(b, b);
        if determinant * determinant < length_A * length_B * 1e-6 then
            determinant = 0;
        end
    end

    return determinant;
end

--! Calculates the dot product of vectors @p a and @p b.
function city_image:dot(a, b)
    return a.x * b.x + a.y * b.y;
end

--! Calculates the vector from point @p a to @p b.
function city_image:vector(a, b)
    return {x = b.x - a.x, y = b.y - a.y};
end

--! Calculates the unit vector for vector @p a.
--! If @p b is given, uses the vector from a to b.
function city_image:unit_vector(a, b)
    local v = b and self:vector(a, b) or a;
    local length = math.sqrt(self:dot(v, v));

    if length <= 0 then
        return {x = 0, y = 0};
    else
        return {x = v.x / length, y = v.y / length};
    end
end

--! Calculates the position of @p point along the line A (a1 -> a2).
function city_image:position_along_line(a1, a2, point)
    local v_A = self:vector(a1, a2);
    local A_squared = self:dot(v_A, v_A)

    local v_a1_P = self:vector(a1, point);
    return self:dot(v_a1_P, v_A) / A_squared;
end

--! @class city_image.polyline
--! A polyline consisting of x-y value pairs.
--!
--! There is no automatic line segment
--! from the last point and the first point.
--!
--! Has methods for area operations and SVG output.
city_image.polyline = {
    --! Outline points, as list of {x = number, y = number}.
    data = {};
    det = city_image.det;
    dot = city_image.dot;
    vector = city_image.vector;
    unit_vector = city_image.unit_vector;
    position_along_line = city_image.position_along_line;
};

--! Creates a new polyline, initialized from @p initialization.
function city_image.polyline:new(initialization)
    local d = {};
    for i, v in ipairs(initialization or self.data) do
        d[i] = {x = v.x or v[1], y = v.y or v[2]};
    end
    return setmetatable({data = d}, {__index = self, __tostring = self.__tostring});
end

--! Appends an outline point to the polygon.
--!
--! @param point Table with elements @c x and @c y.
--! @param options Optional table with these elements:
--! @parblock
--! Optional table with these elements:
--!  * If @c skip_duplicates is true, does not append a duplicate point.
--!  * If @c remove_redundant is true, avoids collinear line segments by
--!    removing the last point. This reduces "spikes" to a length of zero.
--! These options do not change the region described by the polygon.
--! @endparblock
--!
--! @exception _ If @c x or @c y does not coerce to a number.
function city_image.polyline:append(point, options)
    local p = {x = tonumber(point.x) or error("Not a number:" .. tostring(point.x)),
               y = tonumber(point.y) or error("Not a number:" .. tostring(point.y))};

    if options and options.remove_redundant and (#self.data > 1) then
        local second_last, last = self.data[#self.data - 1], self.data[#self.data];

        local dir_now = {x = p.x - last.x, y = p.y - last.y};
        local dir_last = {x = last.x - second_last.x, y = last.y - second_last.y};

        local determinant = dir_now.x * dir_last.y - dir_now.y * dir_last.x;
        local magnitude = dir_now.x * dir_now.x + dir_now.y * dir_now.y
                + dir_last.x * dir_last.x + dir_last.y * dir_last.y;
        local is_collinear = math.abs(determinant / magnitude) < 1e-14;

        if is_collinear then
            table.remove(self.data);
        end
    end

    if options and options.skip_duplicates and (#self.data > 0) then
        local last = self:last();

        local distance = (last.x - p.x) * (last.x - p.x)
                + (last.y - p.y) * (last.y - p.y);
        local magnitude = last.x * last.x + last.y * last.y + p.x * p.x + p.y * p.y;
        local is_duplicate = math.abs(distance / magnitude) < 1e-14;

        if is_duplicate then
            return;
        end
    end
    table.insert(self.data, p);
end

--! Returns point @p i, with cyclic wraparound.
function city_image.polyline:point(i)
    assert(#self.data > 0);
    return self.data[((i - 1) % #self.data) + 1];
end

--! Returns the last point.
function city_image.polyline:last()
    assert(#self.data > 0);
    return self.data[#self.data];
end

--! Maximum extents of the polyline.
--!
--! @returns {xmin, xmax, ymin, ymax} as table.
function city_image.polyline:bounds()
    local bounds = {xmin = math.huge, xmax = -math.huge,
                    ymin = math.huge, ymax = -math.huge};

    for _, p in ipairs(self.data) do
        bounds.xmin = math.min(bounds.xmin, p.x);
        bounds.xmax = math.max(bounds.xmax, p.x);
        bounds.ymin = math.min(bounds.ymin, p.y);
        bounds.ymax = math.max(bounds.ymax, p.y);
    end

    return bounds;
end

--! Returns this polyline as SVG path data string.
function city_image.polyline:to_pathdata_string()
    if (#self.data < 2) then
        return "";
    end

    local point_strings = {};
    for _, point in ipairs(self.data) do
        table.insert(point_strings, string.format("%.2f %.2f", point.x, point.y));
    end

    return "M " .. table.concat(point_strings, " L ");
end

--! Transforms this polyline by @p matrix.
--!
--! The matrix operation is `(x' * k, y' * k, k) = (x, y, 1) * matrix`.
--!
--! The matrix has elements @c m11 to @c m33. (@c m<row><column>)
function city_image.polyline:transform(matrix)
    for _, p in ipairs(self.data) do
        -- Interesting: Assigning a new table to an iterator variable name
        -- seems to disconnect it from the actual table instead of changing
        -- the table. --doxydoxy, 2021-08-21
        local pt = city_image.transform_point(p, matrix);
        p.x, p.y = pt.x, pt.y
    end
end

--! Returns a copy of this polyline transformed by @p matrix.
--! @see transform()
function city_image.polyline:transformed(matrix)
    local result = self:new();
    result:transform(matrix);
    return result;
end

--! Returns a copy of this polyline clipped to @p bounds.
function city_image.polyline:clipped(bounds)
    -- This algorithm is simpler than that in polygon.clipped(),
    -- because spikes formed by begin and end do not have a meaning,
    -- and therefore do not need to be removed by rotating through append().
    -- In addition, the whole order needs to be preserved in a polyline,
    -- so rotating is not possible anyway.
    --
    -- This means, every point can be added in the order
    -- returned by points_clipped_at().

    local points = {};
    for point in self:points_clipped_at(bounds) do
        table.insert(points, point);
    end

    -- However, loops around `bounds` are not detected by
    -- this simplified algorithm, if they are at the begin or end.
    -- Because these loops do not connect points within `bounds`.
    -- they are not meaningful.

    -- Remove meaningless loops around `bounds` from `points`.

    while #points > 1 do
        if points[1].q ~= 0 and points[2].q ~= 0 then
            table.remove(points, 1);
        else
            break;
        end
    end

    while #points > 1 do
        if points[#points].q ~= 0 and points[#points - 1].q ~= 0 then
            table.remove(points);
        else
            break;
        end
    end

    -- Add loopless points to a new polyline.
    local result = self:new({});
    for _, point in ipairs(points) do
        result:append(point, {skip_duplicates = true, remove_redundant = false});
    end

    if #result.data < 2 then
        -- This case can occur if the polyline touches the limits
        -- of `bounds` in only one point.
        return self:new({});
    else
        return result;
    end
end

--! Returns an iterator through all points,
--! clipped to @p bounds, with extra points ("crossings") inserted
--! where the path enters or leaves @p bounds.
--!
--! The returned points may be redundant
--! and even form spikes which were not there before,
--! but these redundancies can be removed with the capabilities of append().
--!
--! Each point is returned with an @c q element, see points_crossed_at().
--!
--! For @p options, see points_crossed_at().
function city_image.polyline:points_clipped_at(bounds, options)
    assert(bounds.xmin <= bounds.xmax);
    assert(bounds.ymin <= bounds.ymax);

    --! Returns the corner of @c bounds that corresponds to @p quadrant.
    --!
    --! If @p quadrant is not in {1, 2, 3, 4}, returns @c nil.
    local function corner(quadrant)
        if (quadrant == 1) then
            return {x = bounds.xmax, y = bounds.ymax};
        elseif (quadrant == 2) then
            return {x = bounds.xmin, y = bounds.ymax};
        elseif (quadrant == 3) then
            return {x = bounds.xmin, y = bounds.ymin};
        elseif (quadrant == 4) then
            return {x = bounds.xmax, y = bounds.ymin};
        end
    end

    local get_crossed_points = self:points_crossed_at(bounds, options);

    local function iterator()
        local point = get_crossed_points();

        if not point then
            -- End.
            return nil;
        elseif point.q == 0 then
            -- Return all points from quadrant 0.
            return point;
        elseif point.q == 1 or point.q == 2 or point.q == 3 or point.q == 4 then
            -- Return all points from quadrants {1, 2, 3, 4} as corners,
            -- so movements outside of `bounds` cause loops around `bounds`.
            local c = corner(point.q);
            c.q = point.q;
            return c;
        else
            -- Do not return points from quadrants {1.5, 2.5, 3.5, 4.5},
            -- because these *will be* redundant to the next point.
            -- Explanation: These quadrants are strictly outside of `bounds`,
            -- and as soon as the path goes back into `bounds`,
            -- a "crossing" on the border will occur (which is in quadrant 0).

            -- Return next point.
            return iterator();
        end
    end

    return iterator;
end

--! Returns an iterator through all points ("corners"),
--! with extra points ("crossings") inserted where the segments cross
--! one of the boundaries.
--!
--! @par Quadrants
--! @parblock
--! Returned points have a @c q element which specifies in which quadrant
--! the point is with respect to @p bounds.
--! Quadrant 0 is within @p bounds, the others are as follows:
--!
--! @code
--!  2   1.5   1
--! 2.5   0   4.5
--!  3   3.5   4
--! @endcode
--! (x to the right, y up)
--!
--! Quadrants {1, 2, 3, 4} have inclusive borders, so every crossing that
--! is not on the border of @p bounds, is in one of those quadrants.
--!
--! Quadrants {1.5, 2.5, 3.5, 4.5} have exclusive borders,
--! so only corners can be in these quadrants.
--!
--! Quadrant 0 has inclusive borders, except the four corners.
--!
--! (Yes, these are nonants, but they are numbered like quadrants.)
--! @endparblock
--!
--! @param bounds Table with @c xmin, @c xmax, @c ymin, @c ymax elements.
--! @param options
--!     Optional table with field @c loop.
--!     If @c loop is true, returns crossings between last and first corner
--!     after the last corner has been returned. This is used for polygons.
--!
--! @returns point, inside_clip
function city_image.polyline:points_crossed_at(bounds, options)
    local loop = options and options.loop;

    -- Corners are points from self.data.
    -- Crossings are intersections with lines of `bounds`.

    --! Returns the crossings of the line through a and b,
    --! in the direction a -> b.
    local function calculate_crossings(a, b)
        --! Direction vector.
        local d = self:vector(a, b);
        if (d.x == 0 and d.y == 0) then
            -- There are no crossings.
            return {};
        end

        -- Component-wise crossings with `bounds`.
        -- Field `o`: Crossing with which limit? 'xlim' or 'ylim'.
        -- Field `t`: Position between (if in [0, 1]) `a` and `b`.
        local all_crossings = {};

        -- Crossings with vertical lines:
        if d.x ~= 0 then
            local function y_at_x(x)
                return (x - a.x) * d.y / d.x + a.y;
            end
            table.insert(all_crossings,
                         {x = bounds.xmin, y = y_at_x(bounds.xmin), o = 'xlim'});
            table.insert(all_crossings,
                         {x = bounds.xmax, y = y_at_x(bounds.xmax), o = 'xlim'});
        end

        -- Crossings with horizontal lines:
        if d.y ~= 0 then
            local function x_at_y(y)
                return (y - a.y) * d.x / d.y + a.x;
            end
            table.insert(all_crossings,
                         {x = x_at_y(bounds.ymin), y = bounds.ymin, o = 'ylim'});
            table.insert(all_crossings,
                         {x = x_at_y(bounds.ymax), y = bounds.ymax, o = 'ylim'});
        end

        -- Positions between `a` and `b`:
        local function t_at(p)
            return ((p.x - a.x) * d.x + (p.y - a.y) * d.y) / (d.x * d.x + d.y * d.y);
        end
        for _, p in ipairs(all_crossings) do
            p.t = t_at(p);
        end

        -- Sort in a -> b direction.
        table.sort(all_crossings, function(a, b) return a.t < b.t; end);

        -- Return crossings between `a` and `b`:
        local inner_crossings = {};
        for _, c in ipairs(all_crossings) do
            -- Use noninclusive compare, so fewer points are duplicated.
            if c.t > 0 and c.t < 1 then
                table.insert(inner_crossings, c);
            end
        end
        return inner_crossings;
    end

    --! Adds the @c q element to @p point.
    --!
    --! Quadrants {1, 2, 3, 4} have inclusive borders.
    local function add_quadrant(point)
        if (point.x >= bounds.xmax and point.y >= bounds.ymax) then
            point.q = 1;
        elseif (point.x <= bounds.xmin and point.y >= bounds.ymax) then
            point.q = 2;
        elseif (point.x <= bounds.xmin and point.y <= bounds.ymin) then
            point.q = 3;
        elseif (point.x >= bounds.xmax and point.y <= bounds.ymin) then
            point.q = 4;
        elseif (point.y > bounds.ymax) then
            point.q = 1.5;
        elseif (point.x < bounds.xmin) then
            point.q = 2.5;
        elseif (point.y < bounds.ymin) then
            point.q = 3.5;
        elseif (point.x > bounds.xmax) then
            point.q = 4.5;
        else
            point.q = 0;
        end

        return point;
    end

    local i_next_corner_to_return = 1;
    local crossings_until_next_corner = {};

    local function iterator()
        if #crossings_until_next_corner ~= 0 then
            -- Crossings left, return one of them.
            return add_quadrant(table.remove(crossings_until_next_corner, 1));
        else
            -- No crossings left, calculate next crossings.
            if i_next_corner_to_return > #self.data then
                -- First point reached again, polygon closed.
                return nil;
            end

            -- Get next and following corner.
            -- Use self:point(), because the following corner may be the first.
            local corner = self:point(i_next_corner_to_return);
            local following_corner = self:point(i_next_corner_to_return + 1);

            if i_next_corner_to_return < #self.data or loop then
                -- Calculate crossings up to the following corner.
                crossings_until_next_corner
                        = calculate_crossings(corner, following_corner);
            end

            -- Return next corner.
            i_next_corner_to_return = i_next_corner_to_return + 1;
            return add_quadrant(corner);
        end
    end

    return iterator;
end

--! Returns the intersection point between the infinite line A (a1 -> a2)
--! and the line segment B (b1 -> b2).
--!
--! If line and segment do not intersect, or are parallel, returns @c nil.
--!
--! This function returns a numerically stable number of intersections.
--!
--! This function returns no intersection if either end of B is @e on A.
--!
--! B may have zero length. Behavior is undefined if A has zero length.
--!
--! @returns t_A (position along A), point
function city_image.polyline:line_segment_intersection(a1, a2, b1, b2)
    -- Determining whether there is an intersection can be done in a numerically
    -- stable way by calculating whether b1 and b2 are left or right to A.
    local v_A = self:vector(a1, a2);
    local v_a1_b1 = self:vector(a1, b1);
    local v_a1_b2 = self:vector(a1, b2);
    local det_A_b1 = self:det(v_A, v_a1_b1, {round = true});
    local det_A_b2 = self:det(v_A, v_a1_b2, {round = true});

    if (det_A_b1 > 0 and det_A_b2 < 0) or (det_A_b1 < 0 and det_A_b2 > 0) then
        -- Intersect. Calculate t_A = det(B, a1_b1) / det(B, A).
        local v_B = self:vector(b1, b2);
        local det_B_A = self:det(v_B, v_A);
        local det_B_a1_b1 = self:det(v_B, v_a1_b1);
        local t_A = det_B_a1_b1 / det_B_A;

        local point = {x = a1.x + v_A.x * t_A; y = a1.y + v_A.y * t_A};
        return t_A, point;
    end

    return nil;
end

--! Returns a list of t_A, which are positions in the infinite line A (a1 -> a2),
--! where it intersects with finite line segments of this polyline.
--!
--! Where A touches the polyline in a corner without crossing it,
--! no intersection is returned.
--! Whenever A changes the side of the polyline, one intersection is returned.
--!
--! @param a1 Point on A at t_A = 0.
--! @param a2 Point on A at t_A = 1.
--! @param options
--!     Optional table with field @c loop.
--!     If @c loop is true, returns crossings between last and first corner
--!     after the last segment has been processed. This is used for polygons.
--!
--! @returns Crossing positions along A, in ascending order.
function city_image.polyline:intersections_with_infinite_line(a1, a2, options)
    local valid_intersections = {};

    -- Intersections can happen either within a segment or in a corner
    -- between two segments.
    -- The algorithm here is thought to be numerically stable.

    -- Find intersections within segments:
    do
        local last_segment = (options and options.loop) and #self.data or #self.data - 1;

        for i_segment = 1, last_segment do
            local t_A = self:line_segment_intersection(a1, a2, self:point(i_segment), self:point(i_segment + 1));
            table.insert(valid_intersections, t_A);
        end
    end

    -- Find intersections on corners.
    -- This needs to be done separately, because line_segment_intersection()
    -- has a good reason not to return intersections on corners.
    -- A corner is only an intersection with A if the previous corner is
    -- on the other side of A than the next corner.
    --
    -- This also implies that corners which only touch A are not considered
    -- intersections. Same for endpoints of polylines.

    local v_A = self:vector(a1, a2);

    local A_squared = self:dot(v_A, v_A)
    --! Returns the position on line A that is closed to a corner.
    local function t_A_from_corner(corner_number)
        local corner = self:point(corner_number);
        local v_a1_P = self:vector(a1, corner);
        return self:dot(v_a1_P, v_A) / A_squared;
    end

    -- This algorithm iterates through corners, checks if they are left of A,
    -- on A, or right of A.
    -- If a sequence of left-(on-)+-right (or inverse) is found,
    -- this is an intersection.
    local second_last_side, last_side;
    local total_corners = #self.data
    local last_corner = (options and options.loop) and total_corners * 2 - 1 or total_corners;
    for i_corner = 1, last_corner do
        local v_a1_corner = self:vector(a1, self:point(i_corner));
        local determinant = self:det(v_A, v_a1_corner, {round = true});

        -- Check on which side of A this corner is:
        local side;
        if determinant < 0 then
            side = "left";
        elseif determinant == 0 then
            side = "on";
        else
            side = "right";
        end

        -- Check if this corner ends a left-on-right or right-on-left sequence:
        if (second_last_side == "left" and last_side == "on" and side == "right")
                or (second_last_side == "right" and last_side == "on" and side == "left") then
            -- This sequence is an intersection.
            local t_A = t_A_from_corner(i_corner - 1);
            table.insert(valid_intersections, t_A);
        end

        -- In case of options.loop, we run through the corner list a second time,
        -- to catch intersecting corners wrapping around the point list end.
        -- Exit as soon as a corner may end the wrapping sequence.
        if (side == "left" or side == "right") and i_corner > total_corners then
            break;
        end

        -- Store corner for next iteration:
        if side == "on" and last_side == "on" then
            -- Skip duplicate "on", so left-on-on-on-right is also recognized.
        else
            second_last_side, last_side = last_side, side;
        end
    end

    -- Sort intersections along A:
    table.sort(valid_intersections);

    return valid_intersections;
end

--! Creates a new polyline that is outset by @p width to the left.
--! Outer corners are beveled.
function city_image.polyline:outset(width)
    local pl = self.data;
    local outset_line = self:new({});

    -- Calculates outset vector for a line segment.
    local function outset_vector(segment)
        local v = self:unit_vector(pl[segment], pl[segment + 1]);
        v.x, v.y = v.y * width, -v.x * width;
        return v;
    end

    -- Calculates outset corner point for a corner and the segment
    -- before the corner or after the corner, and the left or right side.
    local function outset_point(corner, segment, side)
        local si = side == "left" and 1 or -1;
        local sg = segment == "before" and corner - 1 or corner;
        local sv = outset_vector(sg);
        local c = pl[corner];
        return {x = c.x + sv.x * si, y = c.y + sv.y * si};
    end

    -- Start point:
    local outset_vector_start = outset_vector(1);
    outset_line:append({x = pl[1].x + outset_vector_start.x, y = pl[1].y + outset_vector_start.y});

    -- Iterate through corners:
    for corner = 2, #pl - 1 do
        -- Check if outset line segments intersect:
        local a1 = outset_point(corner - 1, "after", "left");
        local a2 = outset_point(corner, "before", "left");
        local b1 = outset_point(corner, "after", "left");
        local b2 = outset_point(corner + 1, "before", "left");
        local t_A, p = self:line_segment_intersection(a1, a2, b1, b2);

        if t_A and t_A >= 0 and t_A <= 1 then
            -- Intersect, add intersection point.
            outset_line:append(p, {skip_duplicates = true, remove_redundant = false});
        else
            -- Do not intersect, add both outset points.
            outset_line:append(a2, {skip_duplicates = true, remove_redundant = false});
            outset_line:append(b1, {skip_duplicates = true, remove_redundant = false});
        end
    end

    -- End point:
    local outset_vector_end = outset_vector(#pl - 1);
    outset_line:append({x = pl[#pl].x + outset_vector_end.x, y = pl[#pl].y + outset_vector_end.y});

    return outset_line;
end

function city_image.polyline:__tostring()
    return "Polyline(" .. #self.data .. " points) (" .. self:to_pathdata_string() .. ")";
end

--! @class city_image.polygon
--! A subclass of city_image.polyline, which implicitly has a closing line.
--!
--! This class has additional methods for the area,
--! and some overriden methods.
city_image.polygon = city_image.polyline:new();

--! Returns this polygon as SVG path data string.
function city_image.polygon:to_pathdata_string()
    local polyline_pathdata_string = city_image.polyline.to_pathdata_string(self);
    if polyline_pathdata_string and polyline_pathdata_string ~= "" then
        return polyline_pathdata_string .. " Z";
    else
        return "";
    end
end

--! Absolute area of the polygon, assuming it is a simple polygon.
function city_image.polygon:area()
    if #self.data < 3 then
        return 0;
    end

    -- This algorithm is the simplification of calculating
    -- the area below each line segment as right trapezoid.
    local sum = 0;
    for i = 1, #self.data do
        sum = sum + self:point(i).x * self:point(i + 1).y - self:point(i + 1).x * self:point(i).y;
    end
    return math.abs(sum) * 0.5;
end

--! Returns a copy of this polygon that does not exceed @p bounds.
--!
--! Points outside of @p bounds are clipped to the corners of @p bounds,
--! and removed if redundant.
--!
--! The result may be a polygon with less than 3 points.
--!
--! @param bounds Table with @c xmin, @c xmax, @c ymin, @c ymax elements.
--!
--! @returns city_image.polygon clipped to @p bounds.
function city_image.polygon:clipped(bounds)
    local result = self:new({});

    -- Algorithm:
    -- Points are added, but clipped to `bounds`.
    -- polygon:append() will remove redundant moves.
    --
    -- This algorithm relies on these rules:
    --  * Quadrants {1, 2, 3, 4} have inclusive borders (with corners).
    --  * Quadrant 0 has inclusive borders without corners.
    --  * polygon:points_clipped_at() returns *all* crossings.
    --  * polygon:append() does not append duplicate points,
    --    and removes redundant movements if requested.

    local pre_quadrant_0 = {};

    for point in self:points_clipped_at(bounds, {loop = true}) do
        -- Until the first point in quadrant 0 is found, points are added to
        -- `pre_quadrant_0` instead, so they can be added with proper redundancy
        -- removal afterwards.
        -- This prevents spikes which occur if the polygon starts outside `bounds`.
        if (point.q ~= 0 and #result.data == 0) then
            table.insert(pre_quadrant_0, point);
        else
            result:append(point, {skip_duplicates = true, remove_redundant = true});
        end
    end

    -- Points from the start before quadrant 0 need to be added even
    -- if there are not any points in quadrant 0.
    -- Otherwise, a rectangle larger than `bounds` would be removed completely.
    for _, p in ipairs(pre_quadrant_0) do
        result:append(p, {skip_duplicates = true, remove_redundant = true});
    end

    -- It is possible that the polygon is outside `bounds`, then return nothing.
    if #result.data < 3 then
        return self:new({});
    end

    -- Remove a possible spike formed by begin and end.
    -- These could not be prevented by polygon:append(),
    -- because the polygon was not closed yet.
    do
        -- From now on, the polygon contains at least 3 points.
        -- And because they were added with polygon:append(), they are not collinear.

        -- Rotate the whole polygon by two points,
        -- so polygon:append() can remove the spike.
        for _ = 1, 2 do
            result:append(result.data[1],
                          {skip_duplicates = true, remove_redundant = true});

            -- Because there were at least three points which are not collinear,
            -- polygon:append() can not remove the first two points.
            -- Instead, they (or their meaning) are now at the end.
            -- Thus, it is safe to remove the first two points.
            table.remove(result.data, 1);
        end
    end

    return result;
end

--! Returns a list of t_A, which are positions in the infinite line A (a1 -> a2),
--! where it intersects with the outline of this polygon.
--!
--! Where A touches the polygon in a corner without entering or leaving it,
--! no intersection is returned.
--! Whenever A enters or leaves the polygon, one intersection is returned.
--!
--! @returns Crossing positions along A, in ascending order.
function city_image.polygon:intersections_with_infinite_line(a1, a2)
    return city_image.polyline.intersections_with_infinite_line(self, a1, a2, {loop = true});
end

--! Creates a new polygon from the stroke outline of @p polyline
--! at a stroke width of @p width.
--!
--! Outer corners are beveled, endpoints are only approximated.
--! No self-intersection checking.
function city_image.polygon:from_stroke(polyline, width)
    local polygon = self:new({});

    -- Calculate outlines:
    local left_outline = polyline:outset(width / 2);
    local right_outline = polyline:outset(-width / 2);

    -- Returns a list of points to be inserted between p1 and p2
    -- to get a somewhat rounded corner.
    local function rounded_end(p1, p2)
        local center = {x = (p1.x + p2.x) / 2, y = (p1.y + p2.y) / 2};
        local outset = self:vector(center, p1);

        local points = {};

        -- Rotate the outset vector in 3 steps of 45 degrees, and add points:
        for _ = 1, 3 do
            local x = outset.x * 0.707 + outset.y * -0.707;
            local y = outset.x * 0.707 + outset.y * 0.707;
            outset.x, outset.y = x, y;
            table.insert(points, {x = center.x + x, y = center.y + y});
        end

        return points;
    end

    -- Use left outline directly:
    for _, p in ipairs(left_outline.data) do
        polygon:append(p, {remove_redundant = true});
    end

    -- Insert rounded end:
    for _, p in ipairs(rounded_end(left_outline:last(), right_outline:last())) do
        polygon:append(p, {remove_redundant = true});
    end

    -- Use right outline reversed:
    for i = #right_outline.data, 1, -1 do
        local p = right_outline.data[i];
        polygon:append(p, {remove_redundant = true});
    end

    -- Insert rounded start:
    for _, p in ipairs(rounded_end(right_outline.data[1], left_outline.data[1])) do
        polygon:append(p, {remove_redundant = true});
    end

    return polygon;
end

function city_image.polygon:__tostring()
    return "Polygon(" .. #self.data .. " points, area: " .. self:area() .. ") (" .. self:to_pathdata_string() .. ")";
end

--! @class city_image.multi_polyline
--! A list of city_image.polyline.
--! Can be used to describe multiple lines in one SVG path element.
--! Actually, this class may also store polygons and other multi_polylines.
--!
--! This class provides those interfaces of city_image.polyline,
--! which handle a shape as a whole.
city_image.multi_polyline = {
    --! List of city_image.polyline objects.
    shapes = {};
};

--! Creates a new multi polyline, initialized from @p initialization.
function city_image.multi_polyline:new(initialization)
    local d = {};
    for i, v in ipairs(initialization or self.shapes) do
        d[i] = v:new();
    end
    return setmetatable({shapes = d}, {__index = self, __tostring = self.__tostring});
end

--! Adds a polyline to this multi polyline.
function city_image.multi_polyline:add_shape(polyline)
    table.insert(self.shapes, polyline:new());
end

--! Maximum extents of the multi_polyline.
--!
--! @returns {xmin, xmax, ymin, ymax} as table.
function city_image.multi_polyline:bounds()
    local bounds = {xmin = math.huge, xmax = -math.huge,
                    ymin = math.huge, ymax = -math.huge};

    for _, s in ipairs(self.shapes) do
        local b = s:bounds();
        bounds.xmin = math.min(bounds.xmin, b.xmin);
        bounds.xmax = math.max(bounds.xmax, b.xmax);
        bounds.ymin = math.min(bounds.ymin, b.ymin);
        bounds.ymax = math.max(bounds.ymax, b.ymax);
    end

    return bounds;
end

--! Returns this multi polyline as SVG path data string.
function city_image.multi_polyline:to_pathdata_string()
    local shape_strings = {};
    for _, shape in ipairs(self.shapes) do
        table.insert(shape_strings, shape:to_pathdata_string());
    end

    return table.concat(shape_strings, "  ");
end

--! Transforms this multi polyline by @p matrix.
--!
--! The matrix operation is `(x' * k, y' * k, k) = (x, y, 1) * matrix`.
--!
--! The matrix has elements @c m11 to @c m33. (@c m<row><column>)
function city_image.multi_polyline:transform(matrix)
    for _, shape in ipairs(self.shapes) do
        shape:transform(matrix);
    end
end

--! Returns a copy of this multi polyline transformed by @p matrix.
--! @see transform()
function city_image.multi_polyline:transformed(matrix)
    local result = self:new();
    result:transform(matrix);
    return result;
end

--! Returns a copy of this multi polyline clipped to @p bounds.
function city_image.multi_polyline:clipped(bounds)
    local result = self:new({});
    for _, shape in ipairs(self.shapes) do
        local clipped = shape:clipped(bounds);
        if #clipped.data > 0 then
            result:add_shape(clipped);
        end
    end
    return result;
end

--! Returns all intersection points with infinite line A (a1 -> a2)
--! of all subshapes in order along A.
--! Whether subshapes are closed depends on the subshape objects.
function city_image.multi_polyline:intersections_with_infinite_line(a1, a2)
    local intersections = {};

    for _, shape in ipairs(self.shapes) do
        for _, t_A in ipairs(shape:intersections_with_infinite_line(a1, a2)) do
            table.insert(intersections, t_A);
        end
    end

    table.sort(intersections);

    return intersections;
end

--! Returns the total point count of all subshapes.
function city_image.multi_polyline:point_count(a)
    local count = 0;
    for _, s in ipairs(self.shapes) do
        count = count + #s.data;
    end
    return count;
end

function city_image.multi_polyline:__tostring()
    return "Multi_Polyline(" .. #self.shapes .. " shapes, " .. self:point_count() .. " points) (" .. self:to_pathdata_string() .. ")";
end

--! @class city_image.multi_polygon
--! A subclass of city_image.multi_polyline with different semantics.
city_image.multi_polygon = city_image.multi_polyline:new();

--! Sum of the absolute areas of the polygons (assuming they are simple).
function city_image.multi_polygon:area()
    local result = 0;
    for _, shape in ipairs(self.shapes) do
        result = result + shape:area();
    end
    return result;
end

--! Converts all polylines in @p multi_polyline to stroke outlines at @p width,
--! and returns them as new multi_polygon.
function city_image.multi_polygon:from_stroke(multi_polyline, width)
    local pg = self:new({});
    for _, shape in ipairs(multi_polyline.shapes) do
        pg:add_shape(city_image.polygon:from_stroke(shape, width));
    end
    return pg;
end

function city_image.multi_polygon:__tostring()
    return "Multi_Polygon" .. string.sub(city_image.multi_polyline.__tostring(self), 15);
end

--! @class city_image.city
--! Stores the information from one mapdata table row.
--!
--! (This is how I think how to define a class in Lua.)
city_image.city = {
    --! @public Like "Spawn" or "Trisiston".
    name = "";
    --! @public Like "city" or "village".
    class = "no_class";
    --! @public Outline shape.
    outline = city_image.multi_polygon:new({});
    --! @public Cached value of outline:bounds(). Clear this to mark as dirty.
    bounds_cache = nil;
};

function city_image.city:new(name, class, outline)
    return setmetatable({
                         name = name,
                         class = class,
                         -- If requested, copy provided outline.
                         -- Otherwise, copy own outline.
                         outline = (outline or self.outline):new()
                        },
                        {
                         __index = self,
                         __tostring = self.__tostring
                        });
end

--! Returns the bounding rect of this city’s outline quickly.
--! @see bounds_cache
function city_image.city:bounds_cached()
    if not self.bounds_cache then
        self.bounds_cache = self.outline:bounds();
    end

    return self.bounds_cache;
end

--! Transforms the outline of this city by the transformation matrix @p matrix.
function city_image.city:transform(matrix)
    self.outline:transform(matrix);
end

--! Returns a copy of this city with the outline transformed by @p matrix.
function city_image.city:transformed(matrix)
    local result = self:new();
    result:transform(matrix);
    return result;
end

--! Returns a copy of this city with the outline clipped to @p bounds.
function city_image.city:clipped(bounds)
    local result = self:new();
    result.outline = result.outline:clipped(bounds);
    return result;
end

function city_image.city:__tostring()
    local bounds = self.outline:bounds();
    return "City(" .. self.class .. ") \"" .. self.name .. "\" " .. city_image.rect_to_string(bounds) .. " Outline points: " .. self.outline:point_count();
end

--! @class city_image.street
--! A subclass of city_image.city with different semantic.
--!
--! Uses a multi_polyline instead of a multi_polygon as shape.
city_image.street = city_image.city:new("", "no_class", city_image.multi_polyline:new());

function city_image.street:__tostring()
    local city_string = city_image.city.__tostring(self);
    return "Street" .. string.sub(city_string, 5);
end

--! @class city_image.ocean
--! A subclass of city_image.city with different semantic.
city_image.ocean = city_image.city:new("", "no_class");

function city_image.ocean:__tostring()
    local city_string = city_image.city.__tostring(self);
    return "Ocean" .. string.sub(city_string, 5);
end

--! @class city_image.river
--! A subclass of city_image.ocean with different semantic
--! and stroke outline functionality to support painting waves on a stroke area.
--!
--! Uses a multi_polyline instead of a multi_polygon as shape.
city_image.river = city_image.ocean:new("", "no_class", city_image.multi_polyline:new());

--! Converts the multi_polyline of this river to a multi_polygon that
--! strokes the polylines at @p width.
--!
--! This outline is included when transforming the river using transformed().
function city_image.river:add_stroke_outline(width)
    self.stroke_outline_shape = city_image.multi_polygon:from_stroke(self.outline, width);
end

--! Returns the stroke outline previously generated with add_stroke_outline().
function city_image.river:stroke_outline()
    return self.stroke_outline_shape;
end

--! Reimplemented from city_image.city to include @c self.stroke_outline
--! in transformation operations.
function city_image.river:transform(matrix)
    if self.stroke_outline_shape then
        self.stroke_outline_shape:transform(matrix);
    end
    city_image.city.transform(self, matrix);
end

--! Reimplemented from city_image.city to include @c self.stroke_outline
--! in clipping operations.
function city_image.river:clipped(bounds)
    local result = city_image.city.clipped(self, bounds);
    if result.stroke_outline_shape then
        result.stroke_outline_shape = result.stroke_outline_shape:clipped(bounds);
    end
    return result;
end

function city_image.river:__tostring()
    local ocean_string = city_image.ocean.__tostring(self);
    return "River" .. string.sub(ocean_string, 6);
end

--! @class city_image.region
--! A subclass of city_image.city with different semantic.
--!
--! Has some additional attributes which have different
--! purposes depending on the region class:
--! \li @c image_height
--! \li @c stroke_scale E. g. to make city outline strokes smaller than default
--! \li @c param1
--! \li @c param2
city_image.region = city_image.city:new("", "no_class");

--! Reimplemented from city_image.city to accomodate the additional attributes.
--! Additional attributes are passed as strings or @c nil.
function city_image.region:new(name, class, image_height, stroke_scale, param1, param2, outline)
    local r = city_image.city.new(self, name, class, outline);

    r.param1 = param1;
    r.param2 = param2;

    -- Calculate image height:
    if image_height and #image_height > 0 then
        local h = tonumber(image_height);
        if not h then
            error("Invalid image height: " .. image_height);
        else
            r.image_height = tonumber(image_height);
        end
    end

    if stroke_scale and #stroke_scale > 1 then
        local percent = string.find(stroke_scale, "%", 1, --[[ plain ]] true);
        if percent then
            r.stroke_scale = tonumber(string.sub(stroke_scale, 1, percent - 1)) * 0.01;
        else
            r.stroke_scale = tonumber(stroke_scale);
        end

        if not r.stroke_scale then
            error("Invalid stroke scale: " .. stroke_scale);
        end
    end

    r.highlighted_objects = {};
    if class == "region" or class == "region_outlined" then
        r:parse_object_list(param1);
        r:parse_option_list(param2);
    end

    if r.outline:point_count() <= 0 then
        r:calculate_outline()
    end

    return r;
end

--! Parses a list of object names in square brackets and stores them
--! in #highlighted_objects.
function city_image.region:parse_object_list(input)
    self.highlighted_objects = self.highlighted_objects or {};

    -- Make object string iterator
    -- and discard everything up to the first opening bracket:
    local get_next_object = city_image.string_elements(input, 1, #input, "[");
    get_next_object();

    for s, e in get_next_object do
        local object = string.sub(input, s, e);

        local closing_bracket = string.find(object, "]", 1, --[[ plain ]] true);
        if not closing_bracket then
            error("Can not use object name [" .. object);
        end
        object = stringy.strip(string.sub(object, 1, closing_bracket - 1));

        self.highlighted_objects[object] = true;
    end
end

--! Parses a list of options in square brackets and stores them in #options.
function city_image.region:parse_option_list(input)
    self.options = self.options or {};

    -- Make option string iterator
    -- and discard everything up to the first opening bracket:
    local get_next_option = city_image.string_elements(input, 1, #input, "[");
    get_next_option();

    for s, e in get_next_option do
        local option_element = string.sub(input, s, e);

        local closing_bracket = string.find(option_element, "]", 1, --[[ plain ]] true);
        if not closing_bracket then
            error("Can not use option name [" .. option_element);
        end

        option = stringy.strip(string.sub(option_element, 1, closing_bracket - 1));

        if option == "no_waves" then
            self.options.no_waves = true;
        else
            error("Unknown option: [" .. option_element);
        end
    end
end

--! Calculates a common bounding box of highlighted objects
--! and uses that as outline.
function city_image.region:calculate_outline()
    local function find_object(name)
        for _, where in ipairs({city_image.cities, city_image.streets, city_image.oceans, city_image.rivers}) do
            for _, object in ipairs(where) do
                if object.name == name then
                    return object;
                end
            end
        end
    end

    local xmin, xmax, ymin, ymax = math.huge, -math.huge, math.huge, -math.huge;

    for name, _ in pairs(self.highlighted_objects) do
        local object = find_object(name);
        if object then
            local bounds = object:bounds_cached();
            xmin = math.min(xmin, bounds.xmin);
            xmax = math.max(xmax, bounds.xmax);
            ymin = math.min(ymin, bounds.ymin);
            ymax = math.max(ymax, bounds.ymax);
        end
    end

    if xmin < xmax then
        local margin = math.max(xmax - xmin, ymax - ymin) * 0.05;
        xmin = xmin - margin;
        xmax = xmax + margin;
        ymin = ymin - margin;
        ymax = ymax + margin;

        local polygon = city_image.polygon:new();
        polygon:append({x = xmin, y = ymin});
        polygon:append({x = xmin, y = ymax});
        polygon:append({x = xmax, y = ymax});
        polygon:append({x = xmax, y = ymin});

        local multi_polygon = city_image.multi_polygon:new();
        multi_polygon:add_shape(polygon);
        self.outline = multi_polygon;
    end
end

function city_image.region:__tostring()
    return "Region(" .. self.class .. ") \"" .. self.name .. "\" " .. city_image.rect_to_string(bounds) .. " Outline points: " .. self.outline:point_count() .. " Image height: " .. self.image_height .. " Parameter 1: \"" .. self.param1 .. "\" Parameter 2: \"" .. self.param2 .. "\"";
end

--! The "main" function of city_image.
--! Processes the input file as MediaWiki markup,
--! and creates SVG files as city images in the working directory.
--!
--! @exception _ File system access errors.
--! @exception _ Parsing errors in the input file.
--! @exception _ Missing data in the input file.
--!
--! @param cities_file_name Where to read city information from. (Optional, skip with @c "-".)
--! @param streets_file_name Where to read street information from. (Optional, skip with @c "-".)
--! @param water_file_name Where to read water bodies information from. (Optional, skip with @c "-".)
--! @param regions_file_name Where to read regions information from. (Optional.)
function city_image:process_file(cities_file_name, streets_file_name, water_file_name, regions_file_name)
    -- Parse city and street input files.
    local tasks = {};
    if cities_file_name and cities_file_name ~= "-" then
        table.insert(tasks, {file = cities_file_name, fun = self.parse_city_input});
    end
    if streets_file_name and streets_file_name ~= "-" then
        table.insert(tasks, {file = streets_file_name, fun = self.parse_street_input});
    end
    if water_file_name and water_file_name ~= "-" then
        table.insert(tasks, {file = water_file_name, fun = self.parse_water_input});
    end
    if regions_file_name then
        table.insert(tasks, {file = regions_file_name, fun = self.parse_regions_input});
    end

    for _, task in ipairs(tasks) do
        local input_file, error_message = io.open(task.file);
        if not input_file then
            error("Could not open input file " .. task.file .. ": " .. error_message);
        end

        local input_string = input_file:read("*a");
        if not input_string then
            error("Could not read from input file " .. task.file);
        end

        local success, error_message = pcall(task.fun, self, input_string);
        if not success then
            error("Failed to parse the input file " .. task.file .. ": " .. error_message);
        end
    end

    -- Process data.
    self:convert_coordinate_system();

    for index = 1, #self.cities do
        local success, error_message = pcall(self.process_city, self, index);
        if not success then
            error("Failed to create image for city " .. index .. " (" .. tostring(self.cities[index]) .. "): " .. error_message);
        end
    end

    for index = 1, #self.regions do
        local success, error_message = pcall(self.process_region, self, index);
        if not success then
            error("Failed to create image for region " .. index .. " (" .. tostring(self.region[index]) .. "): " .. error_message);
        end
    end
end

local generator = city_image:new();
generator:process_file(arg[1], arg[2], arg[3], arg[4]);
